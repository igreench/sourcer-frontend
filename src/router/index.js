import Vue from 'vue';
import Router from 'vue-router';
import Profile from '@/components/Profile';
import Projects from '@/components/Projects';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
    },
    {
      path: '/',
      name: 'Projects',
      component: Projects,
    },
  ],
});
